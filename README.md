# RIDER SIMULATOR

1. For this project I chose Lumen as my framework.
2. For database, I chose MongoDB, Because MongoDB provides Geo location intersect support also for frequent data
   updating NoSQL is a good choice.
3. For handling driver's continuous ping for current location I used Redis as Caching server to keep updated location into redis.
4. Used Nginx as web server.
5. I have written unit test for User registration and OTP verification.
6. I also tried to maintain all clean code principles.

## Setup

1. Open Terminal and Run `cp .env.example .env`
2. Update your `.env` information like, Database info, etc.
3. Run `composer install`.
4. Update `APP_KEY`.
5. Then Run `php artisan jwt:secret`
6. Then `docker-compose up -d --build`.
7. You will see the project will be up & running.
8. To run the test cases `./vendor/bin/phpunit`
9. ALL DONE !!

## Available Route List

| Route URL                                     | Method Type | Payload                                 | Response    |
|-----------------------------------------------|-------------|-----------------------------------------|-------------|
| http://locahost:8000/api/v1/users             | POST        | `name,phone_no,email,type:rider,driver` | `success`   |
| http://localhost:8000/api/v1/login            | POST        | `phone_no/email`                        | `send OTP`  |
| http://localhost:8000/api/v1/token/verify     | POST        | `OTP token`                             | `JWT TOKEN` |
| http://localhost:8000/api/v1/drivers/location | POST + AUTH | `lat,lng,last_update`                   | `SUCCESS`   | 
| http://localhost:8000/api/v1/trips            | POST + AUTH | `from_lat,from_lng,to_lat,to_lng`       | `SUCCESS`   |
| http://localhost:8000/api/v1/trips            | PUT + AUTH  | `status,assigned_user`                  | `SUCCESS`   |

## Things can be done for further development

* Add DTO for better Type Hint experience in Database.
* Implement SMS/EMAIL for sending OTP.
* Complete Assign driver algorithm.
* Implement Geo location based circle implementation for searching driver those who intersect within the radius.
* Write Test case for every scenario.
