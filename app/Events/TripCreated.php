<?php

namespace App\Events;

use App\Models\RiderTrip;

class TripCreated extends Event
{

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(public RiderTrip $riderTrip)
    {
    }
}
