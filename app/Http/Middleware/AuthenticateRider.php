<?php

namespace App\Http\Middleware;

use App\Constants\UserType;
use Closure;

class AuthenticateRider
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!auth()->user()|| auth()->user()->type !== UserType::RIDER) {
            return response('Unauthorized.', 401);
        }
        return $next($request);
    }
}
