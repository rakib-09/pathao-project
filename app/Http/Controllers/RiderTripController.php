<?php

namespace App\Http\Controllers;

use App\Constants\Strings;
use App\Constants\TripStatus;
use App\Services\RiderTripService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class RiderTripController extends Controller
{
    public function __construct(private RiderTripService $service)
    {
        $this->middleware('rider.auth');
    }

    public function create(Request $request): JsonResponse
    {
        $this->validate($request, [
            'from_lat' => 'required|string',
            'from_lng' => 'required|string',
            'to_lat' => 'required|string',
            'to_lng' => 'required|string'
        ]);

        $this->service
            ->setUser(auth()->user())
            ->setFromLat($request->from_lat)
            ->setFromLng($request->from_lng)
            ->setToLat($request->to_lat)
            ->setToLng($request->to_lng)
            ->createRiderTrip();

        return $this->successResponse(Strings::TRIP_CREATED);
    }

    public function update($id, Request $request): JsonResponse
    {
        $this->validate($request, [
            'status' => ['required', 'string', Rule::in(TripStatus::LIST)],
            'assigned_user' => 'required|string'
        ]);

        $this->service->updateRiderTrip($id);
        return $this->successResponse(Strings::TRIP_UPDATED);
    }
}
