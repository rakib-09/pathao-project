<?php

namespace App\Http\Controllers;

use App\Constants\Strings;
use App\Services\DriverLocationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class DriverLocationController extends Controller
{
    public function __construct(private DriverLocationService $service)
    {
        $this->middleware('driver.auth');
    }

    public function updateDriverLocation(Request $request): JsonResponse
    {
        $this->validate($request, [
            'lat' => 'required|string',
            'lng' => 'required|string',
            'last_update' => 'required|date_format:Y-m-d H:i:s'
        ]);

        $this->service
            ->setUser(auth()->user())
            ->setLat($request->lat)
            ->setLng($request->lng)
            ->setLastUpdated($request->last_update)
            ->updateDriverLocation();

        return $this->successResponse(Strings::LOCATION_UPDATED);
    }
}
