<?php

namespace App\Http\Controllers;

use App\Constants\Strings;
use App\Exceptions\ApiException;
use App\Http\Resources\UserResource;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;


class UserController extends Controller
{

    public function __construct(private UserService $service)
    {
    }

    /**
     * @throws ValidationException
     * @throws RuntimeException
     */
    public function create(Request $request): JsonResponse
    {
        $this->validate($request, [
            'name' => 'required|max:50',
            'phone_no' => 'required|string|unique:users,phone_no',
            'email' => 'required_if:type,rider|email|max:255|unique:users,email',
            'type' => 'required|in:driver,rider'
        ]);

        if (!is_numeric($request->phone_no)) {
            throw new ApiException(Strings::INVALID_NUMBER, 400);
        }
        $user = $this->service
            ->setName($request->name)
            ->setPhoneNo($request->phone_no)
            ->setType($request->type);
        if ($request->has('email')) {
            $user->setEmail($request->email);
        }
        $user = $user->createUser();

        return $this->payload(new UserResource($user), 201);
    }
}
