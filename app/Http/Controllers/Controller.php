<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public function payload(array|JsonResource $data, int $statusCode = 200): JsonResponse
    {
        $data = [
            'status' => 'success',
            'data' => $data
        ];
        return response()->json($data, $statusCode);
    }

    public function successResponse(string $message = 'Request successful.', int $statusCode = 200): JsonResponse
    {
        $data = [
            'status' => 'success',
            'message' => $message
        ];
        return response()->json($data, $statusCode);
    }
}
