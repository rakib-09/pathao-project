<?php

namespace App\Http\Controllers;

use App\Constants\Strings;
use App\Exceptions\ApiException;
use App\Services\AuthService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    public function __construct(private AuthService $service)
    {
        $this->middleware('jwt.auth', ['except' => ['login', 'verifyOtp']]);
    }

    /**
     * @throws ValidationException
     * @throws Exception
     */
    public function login(Request $request): JsonResponse
    {
        $this->validate($request, [
            'phone_no' => 'required_without:email|string',
            'email' => 'required_without:phone_no|email'
        ]);

        if ($request->phone_no && !is_numeric($request->phone_no)) {
            throw new ApiException(Strings::INVALID_NUMBER, 400);
        }

        if ($request->email) {
            $otp = $this->service->setEmail($request->email);
        } else {
            $otp = $this->service->setPhoneNo($request->phone_no);
        }

        if ($otp->sendOtp()) {
            return $this->successResponse(Strings::OTP_SENT);
        }
        throw new ApiException(Strings::SOMETHING_WENT_WRONG, 400);
    }

    /**
     * @throws ValidationException
     */
    public function verifyOtp(Request $request): JsonResponse
    {
        $this->validate($request, [
            'otp_token' => 'required|numeric',
        ]);

        $token = $this->service->setOtpToken($request->otp_token)->verifyOtp();
        return $this->respondWithToken($token);
    }

    private function respondWithToken($token): JsonResponse
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer'
        ]);
    }
}
