<?php

namespace App\Listeners;

use App\Constants\TripStatus;
use App\Events\TripCreated;
use App\Services\DriverLocationService;
use App\Services\RiderTripService;
use Illuminate\Contracts\Queue\ShouldQueue;

class AssignDriver implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(private DriverLocationService $service, private RiderTripService $riderTripService)
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param TripCreated $event
     * @return void
     */
    public function handle(TripCreated $event)
    {
        $tripDetails = $event->riderTrip;
        $pickupLocation = json_decode($tripDetails->pickup_location, false);
        $fromLat = $pickupLocation->lat;
        $fromLng = $pickupLocation->lng;
        $assignedDriver = $this->service->findDriver($fromLat, $fromLng);

        $this->riderTripService
            ->setAssignedDriver($assignedDriver)
            ->setStatus(TripStatus::ACCEPT)
            ->updateRiderTrip($tripDetails->id);
    }
}
