<?php

namespace App\Listeners;

use App\Events\TripStatusUpdated;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyRider implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param TripStatusUpdated $event
     * @return void
     */
    public function handle(TripStatusUpdated $event)
    {
    }
}
