<?php

namespace App\Services;

use App\Constants\Strings;
use App\Constants\TripStatus;
use App\Events\TripCreated;
use App\Events\TripStatusUpdated;
use App\Exceptions\ApiException;
use App\Models\RiderTrip;
use App\Models\User;


class RiderTripService
{
    private User $user;
    private string $fromLat;
    private string $fromLng;
    private string $toLat;
    private string $toLng;
    private string $status;
    private string $assignedDriver;


    public function setUser(User $user): RiderTripService
    {
        $this->user = $user;
        return $this;
    }

    public function setFromLat(string $fromLat): RiderTripService
    {
        $this->fromLat = $fromLat;
        return $this;
    }

    public function setFromLng(string $fromLng): RiderTripService
    {
        $this->fromLng = $fromLng;
        return $this;
    }

    public function setToLat(string $toLat): RiderTripService
    {
        $this->toLat = $toLat;
        return $this;
    }

    public function setToLng(string $toLng): RiderTripService
    {
        $this->toLng = $toLng;
        return $this;
    }

    public function setAssignedDriver(string $assignedDriver): RiderTripService
    {
        $this->assignedDriver = $assignedDriver;
        return $this;
    }

    public function setStatus(string $status): RiderTripService
    {
        $this->status = $status;
        return $this;
    }

    public function createRiderTrip(): RiderTrip
    {
        $riderTrip = RiderTrip::create($this->makeData());
        event(new TripCreated($riderTrip));
        return $riderTrip;
    }

    public function updateRiderTrip($id): RiderTrip
    {
        $riderTrip = RiderTrip::find($id);
        if (!$riderTrip) {
            throw new ApiException(Strings::TRIP_NOT_FOUND);
        }
        $riderTrip->status = $this->status;
        $riderTrip->assigned_driver = $this->assignedDriver;
        $riderTrip->save();
        event(new TripStatusUpdated($riderTrip));
        return $riderTrip;
    }

    private function makeData(): array
    {
        return [
            'user_id' => $this->user->id,
            'pickup_location' => json_encode(['lat' => $this->fromLat, 'lng' => $this->fromLng]),
            'drop_location' => json_encode(['lat' => $this->toLat, 'lng' => $this->toLng]),
            'assigned_user' => null,
            'status' => TripStatus::PENDING
        ];
    }
}
