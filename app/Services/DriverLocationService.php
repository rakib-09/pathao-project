<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Support\Facades\Cache;

class DriverLocationService
{
    private User $user;
    private string $lat;
    private string $lng;
    private string $lastUpdated;

    public function setUser(User $user): DriverLocationService
    {
        $this->user = $user;
        return $this;
    }

    public function setLat(string $lat): DriverLocationService
    {
        $this->lat = $lat;
        return $this;
    }

    public function setLng(string $lng): DriverLocationService
    {
        $this->lng = $lng;
        return $this;
    }

    public function setLastUpdated(string $lastUpdated): DriverLocationService
    {
        $this->lastUpdated = $lastUpdated;
        return $this;
    }

    public function updateDriverLocation(): bool
    {
        Cache::put($this->user->id, json_encode($this->makeData()), 10);
        return true;
    }

    public function findDriver(string $lat, string $lng): string|null
    {
        //search from redis which one intersect and return that driver id and details
        return 'assigned_driver_user_id';
    }

    private function makeData(): array
    {
        return [
            'lat' => $this->lat,
            'lng' => $this->lng,
            'last_updated' => $this->lastUpdated
        ];
    }
}
