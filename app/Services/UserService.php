<?php

namespace App\Services;

use App\Models\User;

class UserService
{
    private string $name;
    private string $email;
    private string $phoneNo;
    private string $type;

    public function setName(string $name): UserService
    {
        $this->name = $name;
        return $this;
    }

    public function setEmail(string $email): UserService
    {
        $this->email = $email;
        return $this;
    }

    public function setPhoneNo(string $phoneNo): UserService
    {
        $this->phoneNo = $phoneNo;
        return $this;
    }

    public function setType(string $type): UserService
    {
        $this->type = $type;
        return $this;
    }

    public function createUser(): User
    {
        return User::create($this->makeData());
    }

    public function getUser(array $condition): User|null
    {
        return User::where($condition)->first();
    }

    private function makeData(): array
    {
        $data = [
            'name' => $this->name,
            'phone_no' => $this->phoneNo,
            'type' => $this->type
        ];
        if (isset($this->email)) {
            $data['email'] = $this->email;
        }
        return $data;
    }
}
