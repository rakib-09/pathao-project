<?php

namespace App\Services;

use App\Constants\Strings;
use App\Exceptions\ApiException;
use App\Models\UserOTP;


class UserOTPService
{
    private string $userId;
    private string $otpToken;

    public function setUserId(string $userId): UserOTPService
    {
        $this->userId = $userId;
        return $this;
    }

    public function setOtpToken(string $otpToken): UserOTPService
    {
        $this->otpToken = $otpToken;
        return $this;
    }

    public function create(): UserOTP
    {
        return UserOTP::create([
            'user_id' => $this->userId,
            'otp_number' => $this->otpToken,
            'is_verified' => false
        ]);
    }

    public function verifyOtp(): UserOTP
    {
        $userOTP = UserOTP::where('otp_number', $this->otpToken)
            ->where('is_verified', false)
            ->first();
        if (!$userOTP) {
            throw new ApiException(Strings::INVALID_OTP, 400);
        }
        $userOTP->is_verified = true;
        $userOTP->save();
        return $userOTP;
    }
}
