<?php

namespace App\Services;

use App\Constants\Strings;
use App\Exceptions\ApiException;
use App\Models\User;
use App\Services\OTP\EmailOTPService;
use App\Services\OTP\OTPContract;
use App\Services\OTP\OTPManager;
use App\Services\OTP\SmsOTPService;
use Exception;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthService
{
    private string $phoneNo;
    private string $email;
    private int $otpToken;

    public function __construct(private UserService $userService, private OTPManager $OTPManager)
    {
    }

    public function setEmail(string $email): AuthService
    {
        $this->email = $email;
        return $this;
    }

    public function setPhoneNo(string $phoneNo): AuthService
    {
        $this->phoneNo = $phoneNo;
        return $this;
    }

    public function setOtpToken(int $token): AuthService
    {
        $this->otpToken = $token;
        return $this;
    }

    /**
     * @throws Exception
     */
    public function sendOtp(): bool
    {
        $user = $this->fetchUser();
        if (!$user) {
            throw new ApiException(Strings::SOMETHING_WENT_WRONG, 400);
        }
        $contact = $this->email ?? $this->phoneNo;
        $otpContract = $this->OtpContractManager();
        return $this->OTPManager->setUserId($user->id)->setContactDetail($contact)->sendOTP($otpContract);
    }

    public function verifyOtp(): string
    {
        $user = $this->OTPManager->setOTPToken($this->otpToken)->verifyUserByOtp();
        return JWTAuth::fromUser($user);
    }

    private function OtpContractManager(): OTPContract
    {
        if ($this->phoneNo) {
            return new SmsOTPService();
        }
        return new EmailOTPService();
    }

    private function fetchUser(): User|null
    {
        if ($this->phoneNo) {
            return $this->userService->getUser(['phone_no' => $this->phoneNo]);
        }
        return $this->userService->getUser(['email' => $this->email]);
    }
}
