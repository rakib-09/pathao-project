<?php

namespace App\Services\OTP;

interface OTPContract
{
    public function shoot(string $content, string $contactMedium): bool;
}
