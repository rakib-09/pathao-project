<?php

namespace App\Services\OTP;

use App\Constants\Strings;
use App\Jobs\ShootOTPJob;
use App\Models\User;
use App\Models\UserOTP;
use App\Services\UserOTPService;
use App\Services\UserService;
use Exception;

class OTPManager
{
    private string $userId;
    private string $contactDetail;
    private string $OTPToken;


    public function __construct(private UserOTPService $service, private UserService $userService)
    {
    }

    public function setUserId(string $userId): static
    {
        $this->userId = $userId;
        return $this;
    }

    public function setContactDetail(string $contact): OTPManager
    {
        $this->contactDetail = $contact;
        return $this;
    }

    public function setOTPToken(int $otpToken): static
    {
        $this->OTPToken = $otpToken;
        return $this;
    }

    /**
     * @throws Exception
     */
    public function sendOTP(OTPContract $contract): bool
    {
        $this->OTPToken = $this->getToken();
        $text = $this->getText($this->OTPToken);
        dispatch(new ShootOTPJob($contract, $text, $this->contactDetail));
        $this->insertIntoDB();

        return true;
    }

    public function verifyUserByOtp(): User
    {
        $userOtp = $this->service->setOtpToken($this->OTPToken)->verifyOtp();
        return $this->userService->getUser(['_id' => $userOtp->user_id]);
    }

    private function getText(int $otp): string
    {
        return sprintf(Strings::OTP_MSG, $otp);
    }

    /**
     * @throws Exception
     */
    private function getToken(): int
    {
        return random_int(111111, 999999);
    }

    private function insertIntoDB(): UserOTP
    {
        return $this->service->setUserId($this->userId)->setOtpToken($this->OTPToken)->create();
    }

}
