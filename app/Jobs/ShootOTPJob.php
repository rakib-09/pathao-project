<?php

namespace App\Jobs;

use App\Services\OTP\OTPContract;

class ShootOTPJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(private OTPContract $otpContract, private string $text, private string $contactDetails)
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->otpContract->shoot($this->text, $this->contactDetails);
    }
}
