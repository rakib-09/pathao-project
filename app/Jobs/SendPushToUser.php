<?php

namespace App\Jobs;

use App\Models\User;

class SendPushToUser extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(private User $user, private string $msg)
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // send push notification to user
    }
}
