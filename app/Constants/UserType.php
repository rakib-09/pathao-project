<?php

namespace App\Constants;

final class UserType
{
    public const RIDER = 'rider';
    public const DRIVER = 'driver';
}
