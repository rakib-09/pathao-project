<?php

namespace App\Constants;

final class TripStatus
{
    public const PENDING = 'pending';
    public const ACCEPT = 'accept';
    public const START = 'start';
    public const END = 'end';
    public const CANCEL = 'cancel';

    public const LIST = [self::PENDING, self::ACCEPT, self::START, self::END, self::CANCEL];
}
