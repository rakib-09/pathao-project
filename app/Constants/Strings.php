<?php

namespace App\Constants;

final class Strings
{
    public const OTP_MSG = "Your One Time Password is #%s";
    public const OTP_SENT = "One Time Password sent successfully";
    public const TRIP_CREATED = "Your trip has been created. waiting for assigning a driver.";
    public const TRIP_UPDATED = "Trip status updated";
    public const TRIP_NOT_FOUND = "Trip not found";
    public const INVALID_OTP = "Invalid OTP";
    public const LOCATION_UPDATED = "Driver Location Updated";
    public const INVALID_NUMBER = "Invalid Phone number!";
    public const SOMETHING_WENT_WRONG = "Something went wrong! Please try again later";
}
