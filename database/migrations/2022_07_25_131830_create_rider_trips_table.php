<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rider_trips', function (Blueprint $collection) {
            $collection->increments('_id');
            $collection->string('user_id');
            $collection->json('pickup_location');
            $collection->json('drop_location');
            $collection->string('assigned_user');
            $collection->string('status');
            $collection->timestamps();

            $collection->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $collection->foreign('assigned_user')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::collection('rider_trips', function (Blueprint $collection) {
            $collection->drop();
        });
    }
};
