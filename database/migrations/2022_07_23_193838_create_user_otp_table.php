<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_otp', function (Blueprint $collection) {
            $collection->increments('_id');
            $collection->string('user_id');
            $collection->string('otp_number');
            $collection->boolean('is_verified')->default(false);
            $collection->timestamps();

            $collection->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::collection('user_otp', function (Blueprint $collection) {
            $collection->drop();
        });
    }
};
