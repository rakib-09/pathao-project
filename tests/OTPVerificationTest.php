<?php

class OTPVerificationTest extends TestCase
{
    public function test_otp_send_with_invalid_number(): void
    {
        $response = $this->json('post', '/login', ['phone_no' => '01790700919'], $this->commonHeaders());
        $response->assertResponseStatus(400);
    }

    public function test_otp_send(): void
    {
        $this->createUser();
        $userData = $this->userData();
        $response = $this->json('post', '/login', ['phone_no' => $userData['phone_no']], $this->commonHeaders());
        $response->assertResponseStatus(200);
    }

    public function test_otp_exists_in_db(): void
    {
        $user = $this->createUser();
        $userData = $this->userData();
        $response = $this->json('post', '/login', ['phone_no' => $userData['phone_no']], $this->commonHeaders());
        $response->seeInDatabase('user_otp', ['user_id' => $user->id, 'is_verified' => false]);
    }

}
