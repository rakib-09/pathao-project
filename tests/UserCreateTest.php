<?php

class UserCreateTest extends TestCase
{
    public function test_user_creation_and_check_in_database(): void
    {
        $response = $this->json('post', '/users', $this->userData(), $this->commonHeaders());
        $data = $response->response->json(['data']);
        $response->assertResponseStatus(201);
        if ($data) {
            $response->seeInDatabase('users', ['_id' => $data['id']]);
        }
    }

    public function test_failed_duplicate_phone_no_verification(): void
    {
        $this->test_user_creation_and_check_in_database();
        $response = $this->json('post', '/users', $this->userData(), $this->commonHeaders());
        $response->assertResponseStatus(422);
    }


}
