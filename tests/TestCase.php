<?php

use App\Models\User;
use App\Services\UserService;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use DatabaseMigrations;

    protected $baseUrl = 'http://localhost:8000/api/v1';

    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__ . '/../bootstrap/app.php';
    }

    public function commonHeaders(): array
    {
        return [
            "Accept" => "application/json",
            "Content-Type" => "application/json"
        ];
    }

    protected function userData(): array
    {
        return [
            "name" => "rakib",
            "phone_no" => "01790700920",
            "type" => "driver"
        ];
    }

    protected function createUser(): User
    {
        /** @var UserService $userSvc */
        $userSvc = $this->app->make(UserService::class);
        $userData = $this->userData();
        return $userSvc
            ->setName($userData['name'])
            ->setPhoneNo($userData['phone_no'])
            ->setType($userData['type'])
            ->createUser();
    }
}
