<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use App\Http\Controllers\UserController;

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function ($router) {
    $router->group(['prefix' => 'v1'], function ($router){
        $router->post('users', 'UserController@create');
        $router->post('login', 'AuthController@login');
        $router->post('token/verify', 'AuthController@verifyOtp');
        $router->post('drivers/location', 'DriverLocationController@updateDriverLocation');
        $router->post('trips', 'RiderTripController@create');
        $router->put('trips/{id}', 'RiderTripController@update');
    });
});

